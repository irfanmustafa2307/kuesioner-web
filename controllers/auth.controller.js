/* eslint-disable no-plusplus */
/* eslint-disable no-shadow */
/* eslint-disable import/order */
const config = require('../config/auth.config');
const db = require('../models');

const User = db.user;
const Role = db.role;

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

exports.signup = (req, res) => {
  const user = new User({
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
    phone: req.body.phone,
  });

  user.save((err, user) => {
    if (err) {
      return res.status(500).send({
        message: err,
      });
    }

    if (req.body.roles) {
      Role.find({
        name: { $in: req.body.roles },
      }, (err, roles) => {
        if (err) {
          return res.status(500).send({
            message: err,
          });
        }

        user.roles = roles.map((role) => role._id);
        user.save((err) => {
          if (err) {
            return res.status(500).send({
              message: err,
            });
          }

          res.send({
            message: 'User telah berhasil terdaftar',
          });
        });
      });
    } else {
      Role.findOne({ name: 'user' }, (err, role) => {
        if (err) {
          return res.status(500).send({
            message: err,
          });
        }

        user.roles = [role._id];
        user.save((err) => {
          if (err) {
            return res.status(500).send({
              message: err,
            });
          }

          res.send({
            message: 'User telah berhasil terdaftar',
          });
        });
      });
    }
  });
};

exports.signin = (req, res) => {
  User.findOne({
    username: req.body.username,
  }).populate('roles', '-__v').exec((err, user) => {
    if (err) {
      return res.status(500).send({
        message: err,
      });
    }
    if (!user) { // jika user tidak ada
      return res.status(404).send({
        message: 'user tidak terdaftar',
      });
    }

    // jika username ada sekarang membaca password
    const passwordIsvalid = bcrypt.compareSync(
      req.body.password,
      user.password,
    );

    // jika password salah
    if (!passwordIsvalid) {
      return res.status(404).send({
        message: 'password tidak valid',
      });
    }

    const token = jwt.sign({ id: user.id, name: user.username }, config.secret, {
      expiresIn: 86400, // 24 jam
    });

    const authorities = [];

    for (let i = 0; i < user.roles.length; i++) {
      authorities.push(`ROLES_${user.roles[i].name.toUpperCase()}`); // ["ROLE_ADMIN", "ROLE_VENDOR"]
    }

    res.status(200).send({
      id: user._id,
      username: user.username,
      email: user.email,
      roles: user.roles,
      phone: user.phone,
      accessToken: token,
    });
  });
};
