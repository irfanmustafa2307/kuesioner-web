/* eslint-disable no-unused-vars */
// eslint-disable-next-line consistent-return
const mongoose = require('mongoose');
const config = require('../config/auth.config');
const db = require('../models');

const CodeReferences = db.code_references;
const TalentGroups = db.talent_groups;
const Talents = db.talents;
const Forms = db.forms;
const Responds = db.responds;
const User = db.user;
const Role = db.role;

exports.getUser = async (req, res) => {
  try {
    const user = await User.find();
    res.json(user);
  } catch (error) {
    return res.status(500).send({
      message: error,
    });
  }
};

exports.getRole = async (req, res) => {
  try {
    const role = await Role.find();
    res.json(role);
  } catch (error) {
    return res.status(500).send({
      message: error,
    });
  }
};

exports.createRoles = async (req, res) => {
  try {
    const roles = new Role({
      name: req.body.name,
    });

    const result = await roles.save();
    return res.status(200).send({
      data: result,
    });
  } catch (error) {
    return res.status(500).send({
      message: error,
    });
  }
};

exports.createCodeReferences = async (req, res) => {
  try {
    const codeReferences = new CodeReferences({
      code: req.body.code,
      userId: req.userId,
      maxResponds: req.body.maxResponds,
      createdBy: req.userId,
      updatedBy: req.userId,
    });

    const result = await codeReferences.save();
    return res.status(200).send({
      data: result,
    });
  } catch (error) {
    return res.status(500).send({
      message: error,
    });
  }
};

exports.getCodeReferences = async (req, res) => {
  try {
    const codeReferences = await CodeReferences.find();
    res.json(codeReferences);
  } catch (error) {
    return res.status(500).send({
      message: error,
    });
  }
};

exports.createTalentGroups = async (req, res) => {
  try {
    const talentGroups = new TalentGroups({
      name: req.body.name,
      description: req.body.description,
      brainSide: req.body.brainSide,
      createdBy: req.userId,
      updatedBy: req.userId,
    });

    const result = await talentGroups.save();
    return res.status(200).send({
      data: result,
    });
  } catch (error) {
    return res.status(500).send({
      message: error,
    });
  }
};

exports.getTalentGroups = async (req, res) => {
  try {
    const talentGroups = await TalentGroups.find();
    res.json(talentGroups);
  } catch (error) {
    return res.status(500).send({
      message: error,
    });
  }
};

exports.createTalents = async (req, res) => {
  const {
    name, description, groupId, priOrder,
  } = req.body;
  try {
    const talentGroups = await TalentGroups.findOne({ _id: mongoose.Types.ObjectId(groupId) });

    if (talentGroups !== null) {
      const talents = await Talents.create({
        name,
        description,
        priOrder,
        groupId,
        createdBy: req.userId,
        updatedBy: req.userId,
      });

      //   talentGroups.talents.push({ _id: talents._id });
      //   await talentGroups.save()

      //   let result = await talents.save()

      await TalentGroups.updateOne({ _id: mongoose.Types.ObjectId(groupId) }, { $push: { talents: mongoose.Types.ObjectId(talents._id) } });
      return res.status(200).send({ // tampilkan data
        data: talents,
      });
    }
    return res.status(500).send({
      message: 'Data groupId tidak ada',
    });
  } catch (error) {
    return res.status(500).send({
      message: 'Terjadi kesalahan, mohon cek data anda!',
    });
  }
};

exports.getTalents = async (req, res) => {
  try {
    const talents = await Talents.find();
    res.json(talents);
  } catch (error) {
    return res.status(500).send({
      message: error,
    });
  }
};

exports.deleteTalents = async (req, res) => {
  try {
    const talents = await Talents.findOneAndRemove({ _id: mongoose.Types.ObjectId(req.body.talentId) });

    const talentGroup = await TalentGroups.updateMany(
      { },
      { $pull: { talents: mongoose.Types.ObjectId(req.body.talentId) } },
    );

    res.json({
      talents,
      talentGroup,
    });
  } catch (error) {
    return res.status(500).send({
      message: error,
    });
  }
};

exports.createForms = async (req, res) => {
  const { title, questions } = req.body;
  try {
    const forms = await Forms.create({
      title,
      questions: questions.map((q) => {
        q.createdBy = req.userId;
        q.updatedBy = req.userId;
        return q;
      }),

      createdBy: req.userId,
      updatedBy: req.userId,
    });

    const result = await forms.save();
    return res.status(200).send({
      data: result,
    });
  } catch (err) {
    return res.status(500).send({
      message: err,
    });
  }
};

exports.getForms = async (req, res) => {
  try {
    const forms = await Forms.find();
    res.json(forms);
  } catch (error) {
    return res.status(500).send({
      message: error,
    });
  }
};

exports.createResponds = async (req, res) => {
  const {
    name, email, phone, code, isDone, results, responds,
  } = req.body;
  try {
    const newResponds = await Responds.create({
      userId: req.userId,
      name,
      email,
      phone,
      code,
      isDone,
      results,
      responds,
      createdBy: req.userId,
      updatedBy: req.userId,

    });

    const result = await newResponds.save();
    return res.status(200).send({
      data: result,
    });
  } catch (error) {
    return res.status(500).send({
      message: error,
    });
  }
};

exports.getResponds = async (req, res) => {
  try {
    const responds = await Responds.find();
    res.json(responds);
  } catch (error) {
    return res.status(500).send({
      message: error,
    });
  }
};
