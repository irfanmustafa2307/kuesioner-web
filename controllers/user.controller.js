exports.allAccess = (req, res) => {
  res.status(200).send({
    role: 'all',
    message: 'Berhasil terverifikasi',
  });
};

exports.userAccess = (req, res) => {
  res.status(200).send({
    role: 'user',
    message: 'Berhasil terverifikasi',
  });
};

exports.adminAccess = (req, res) => {
  res.status(200).send({
    role: 'admin',
    message: 'Berhasil terverifikasi',
  });
};

exports.vendorAccess = (req, res) => {
  res.status(200).send({
    role: 'vendor',
    message: 'Berhasil terverifikasi',
  });
};
