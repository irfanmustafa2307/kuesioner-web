const mongoose = require('mongoose');

const { ObjectId } = mongoose.Schema;

const schema = new mongoose.Schema({
  title: String,
  questions: [
    {
      talentId: {
        type: ObjectId,
        ref: 'Talents',
      },
      description: String,
      reverseFlag: {
        type: Boolean,
        default: false,
      },
      createdBy: {
        type: ObjectId,
        ref: 'User',
      },
      updatedBy: {
        type: ObjectId,
        ref: 'User',
      },
    },
    {
      timestamps: true,
    },
  ],
  createdBy: {
    type: ObjectId,
    ref: 'User',
  },
  updatedBy: {
    type: ObjectId,
    ref: 'User',
  },
},
{
  timestamps: true,
});

const Forms = mongoose.model(
  'Forms', schema, 'forms',
);

module.exports = Forms;
