const db = {};

db.user = require('./user.model');
db.role = require('./role.model');
db.code_references = require('./code_references.model');
db.talent_groups = require('./talent_groups.model');
db.talents = require('./talents.model');
db.forms = require('./forms.model');
db.responds = require('./responds.model');

db.ROLES = ['user', 'admin', 'vendor'];

module.exports = db;
