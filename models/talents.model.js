const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  name: String,
  description: String,
  priOrder: Number,
  groupId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'TalentsGroup',
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
},
{
  timestamps: true,
});

const Talents = mongoose.model(
  'Talents', schema, 'talents',
);

module.exports = Talents;
