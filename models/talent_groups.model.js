const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  name: String,
  description: String,
  brainSide: String,
  talents: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Talents',
    },
  ],
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
},
{
  timestamps: true,
});

const TalentGroups = mongoose.model(
  'TalentGroups', schema, 'talent_groups',
);

module.exports = TalentGroups;
