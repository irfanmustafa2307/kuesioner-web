const mongoose = require('mongoose');

const { ObjectId } = mongoose.Schema;

const schema = new mongoose.Schema({
  userId: {
    type: ObjectId,
    ref: 'User',
  },
  name: String,
  email: String,
  phone: String,
  code: String,
  isDone: {
    type: Boolean,
    default: false,
  },
  results: [{
    talentId: {
      type: ObjectId,
      ref: 'Talents',
    },
    score: Number,
    totalQuestion: Number,
  }],
  responds: [{
    talentId: {
      type: ObjectId,
      ref: 'Talents',
    },
    questionId: {
      type: ObjectId,
      ref: 'Forms',
    },
    value: Number,
  }],
  createdBy: {
    type: ObjectId,
    ref: 'User',
  },
  updatedBy: {
    type: ObjectId,
    ref: 'User',
  },
},
{
  timestamps: true,
});

const Responds = mongoose.model(
  'Responds', schema, 'responds',
);

module.exports = Responds;
