const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  code: String,
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  maxResponds: Number,
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },

},
{
  timestamps: true,
});

const CodeReferences = mongoose.model(
  'CodeReferences', schema, 'code_references',

);

module.exports = CodeReferences;
