/* eslint-disable func-names */
const { authJwt } = require('../middlewares');
const controller = require('../controllers/user.controller');

module.exports = function (app) {
  app.use((req, res, next) => {
    res.header(
      'Access-Control-Allow-headers',
      'Authorization, Origin, Content-Type, Accept',
    );
    next();
  });

  app.get(
    '/api/test/all',
    controller.allAccess,
  );

  app.get(
    '/api/test/user',
    [authJwt.verifyToken],
    controller.userAccess,
  );

  app.get(
    '/api/test/admin',
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.adminAccess,
  );

  app.get(
    '/api/test/vendor',
    [authJwt.verifyToken, authJwt.isVendor],
    controller.vendorAccess,
  );
};
