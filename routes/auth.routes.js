/* eslint-disable func-names */
const { verifySignUp } = require('../middlewares');
const controller = require('../controllers/auth.controller');

module.exports = function (app) {
  app.use((req, res, next) => {
    res.header(
      'Access-Control-Allow-headers',
      'Authorization, Origin, Content-Type, Accept',
    );
    next();
  });

  app.post(
    '/api/auth/signup',
    [
      verifySignUp.checkDuplicateUsernameOrEmail,
      verifySignUp.checkRoleExisted,
    ],
    controller.signup,
  );

  app.post(
    '/api/auth/signin',
    controller.signin,
  );
};
