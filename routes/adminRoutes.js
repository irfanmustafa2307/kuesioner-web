/* eslint-disable func-names */
const { authJwt, verifyCodeReferences } = require('../middlewares');
const controller = require('../controllers/adminControllers');

module.exports = function (app) {
  app.use((req, res, next) => {
    res.header(
      'Access-Control-Allow-headers',
      'Authorization, Origin, Content-Type, Accept',
    );
    next();
  });
  // API Code Refrences
  app.get('/api/v1/code', controller.getCodeReferences);
  app.post('/api/v1/code/create', [authJwt.verifyToken, verifyCodeReferences.checkDuplicateCode], controller.createCodeReferences);

  // GET User
  app.get('/api/v1/users', controller.getUser);
  // GET Role
  app.post('/api/v1/roles/create', controller.createRoles);
  app.get('/api/v1/roles', controller.getRole);
  // API Talent Groups
  app.get('/api/v1/talent-groups', controller.getTalentGroups);
  app.post('/api/v1/talent-groups/create', [authJwt.verifyToken], controller.createTalentGroups);
  // API Talents
  app.post('/api/v1/talents/create', [authJwt.verifyToken], controller.createTalents);
  app.get('/api/v1/talents', controller.getTalents);
  app.post('/api/v1/talents/delete', [authJwt.verifyToken], controller.deleteTalents);
  // API Forms
  app.post('/api/v1/forms/create', [authJwt.verifyToken], controller.createForms);
  app.get('/api/v1/forms', controller.getForms);
  // API Responds
  app.post('/api/v1/responds/create', [authJwt.verifyToken], controller.createResponds);
  app.get('/api/v1/responds', controller.getResponds);
};
