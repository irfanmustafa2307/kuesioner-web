/* eslint-disable eqeqeq */
/* eslint-disable no-shadow */
/* eslint-disable no-use-before-define */
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
require('dotenv').config();

const app = express();
const db = require('./models');
// connect model
const Role = db.role;

const corsOption = {
  origin: 'http://localhost:8081',
};

app.use(cors(corsOption));

mongoose.connect(`${process.env.MONGO_URI}`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
}).then(() => {
  console.log('Berhasil terkoneksi');
  init();
}).catch((err) => {
  console.error('Gagal', err);
  process.exit();
});

function init() {
  Role.estimatedDocumentCount((err, count) => {
    if (!err && count == 0) {
      new Role({
        name: 'user',
      }).save((err) => {
        if (err) {
          console.log('error', err);
        }
        console.log('berhasil menambah role user');
      });

      new Role({
        name: 'admin',
      }).save((err) => {
        if (err) {
          console.log('error', err);
        }
        console.log('berhasil menambah role admin');
      });

      new Role({
        name: 'vendor',
      }).save((err) => {
        if (err) {
          console.log('error', err);
        }
        console.log('berhasil menambah role vendor');
      });
    }
  });
}

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.json({ message: 'Hallo api berjalan dengan lancar' });
});

require('./routes/auth.routes')(app);
require('./routes/user.routes')(app);
require('./routes/adminRoutes')(app);

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`server is running on ${PORT}`);
});
