/* eslint-disable import/extensions */
/* eslint-disable no-plusplus */
/* eslint-disable no-shadow */
/* eslint-disable no-undef */

const jwt = require('jsonwebtoken');
const config = require('../config/auth.config.js');
const db = require('../models');

const User = db.user;
const Role = db.role;

verifyToken = (req, res, next) => {
  const authHeader = req.headers.authorization;
  const token = authHeader.split(' ')[1];

  if (!token) {
    return res.status(403).send({
      message: 'Unauthorized!',
    });
  }
  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: 'Unauthorized!',
      });
    }
    req.userId = decoded.id;
    // console.log(decoded)
    next();
  });
};

isAdmin = (req, res, next) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      return res.status(500).send({
        message: err,
      });
    }

    Role.find({
      _id: { $in: user.roles },
    },
    (err, roles) => {
      if (err) {
        return res.status(500).send({
          message: err,
        });
      }

      for (let i = 0; i < roles.length; i++) {
        if (roles[i].name === 'admin') {
          next();
          return;
        }
      }

      return res.status(403).send({
        message: 'Hak akses anda tidak cukup',
      });
    });
  });
};

isVendor = (req, res, next) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      return res.status(500).send({
        message: err,
      });
    }

    Role.find({
      _id: { $in: user.roles },
    },
    (err, roles) => {
      if (err) {
        return res.status(500).send({
          message: err,
        });
      }

      for (let i = 0; i < roles.length; i++) {
        if (roles[i].name === 'vendor') {
          next();
          return;
        }
      }

      return res.status(403).send({
        message: 'Hak akses anda tidak cukup',
      });
    });
  });
};

const authJwt = {
  verifyToken,
  isAdmin,
  isVendor,
};

module.exports = authJwt;
