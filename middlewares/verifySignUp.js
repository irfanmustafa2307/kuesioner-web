/* eslint-disable no-plusplus */
/* eslint-disable no-shadow */
/* eslint-disable no-undef */
const db = require('../models');

const { ROLES } = db;
const User = db.user;

checkDuplicateUsernameOrEmail = (req, res, next) => {
  User.findOne({
    username: req.body.username,
  }).exec((err, user) => {
    if (err) {
      res.status(500).send({
        message: err,
      });
      return;
    }
    if (user) {
      res.status(404).send({
        message: 'Username sudah ada mohon gunakan username yang lain!',
      });
      return;
    }

    User.findOne({
      email: req.body.email,
    }, (err, user) => {
      if (err) {
        res.status(500).send({
          message: err,
        });
        return;
      }
      if (user) {
        res.status(404).send({
          message: 'email sudah ada mohon gunakan email yang lain!',
        });
        return;
      }

      next();
    });
  });
};

checkRoleExisted = (req, res, next) => {
  if (req.body.roles) {
    for (let i = 0; i < req.body.roles.length; i++) {
      if (!ROLES.includes(req.body.roles[i])) {
        res.status(400).send({
          message: `Role ${req.body.roles[i]} tidak ada`,
        });
      }
    }
  }
  next();
};

const verifySignUp = {
  checkDuplicateUsernameOrEmail,
  checkRoleExisted,
};

module.exports = verifySignUp;
