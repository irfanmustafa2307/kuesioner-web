/* eslint-disable no-plusplus */
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
const db = require('../models');

const CodeReferences = db.code_references;
const User = db.user;
const TalentGroups = db.talent_groups;
const Talents = db.talents;

checkDuplicateCode = (req, res, next) => {
  CodeReferences.findOne({
    code: req.body.code,
  }).exec((err, code) => {
    if (err) {
      res.status(500).send({
        message: err,
      });
      return;
    }
    if (code) {
      res.status(404).send({
        message: 'Code sudah ada mohon gunakan code yang lain!',
      });
      return;
    }
    next();
  });
};

checkGroupId = (req, res, next) => {
  const GI = [TalentGroups._id];
  if (req.body.groupId) {
    for (let i = 0; i < req.body.groupId; i++) {
      if (!GI.includes(req.body.groupId)) {
        res.status(400).send({
          message: `Id ${req.body.groupId} tidak ada`,
        });
      }
    }
  }
  next();
};

const verifyCodeReferences = {
  checkDuplicateCode,
  checkGroupId,
  // checkUserExisted
};

module.exports = verifyCodeReferences;
